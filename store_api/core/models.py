from turtle import title
from django.db import models

#orm = object relational mapping
class Category(models.Model):
    title = models.CharField(max_length = 255)

    def __str__(self) -> str:
        return self.title