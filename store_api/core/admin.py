from django.contrib import admin

# Register your models here.
from core.models import Category

admin.site.register(Category)